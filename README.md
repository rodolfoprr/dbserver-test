Thanks DBServer for the challenge! I really enjoyed to develop this app! \o/

I hope that I helped you to answer the third "big" question from [The Hitchhiker's Guide to the Galaxy](https://en.wikiquote.org/wiki/The_Hitchhiker's_Guide_to_the_Galaxy): Where shall we have lunch?

### Demo

http://dbserver-test.azurewebsites.net/

### Things to improve:

- Use Repository in the vote request validation;
- Change express-session to something better to identify the user;
- gulp-prod: minify files; concat files;
- Options to add new, edit and delete restaurants;
- Code to select the restaurant of the day.

### About the code:

I believe that what should be highlighted in the code is the use of [John Papa's Styleguide for AngularJS](https://github.com/johnpapa/angular-styleguide), automated tests and Gulp for automation tasks.

### run app

Download [Node.JS](https://nodejs.org/) and run:

```
npm i

npm i gulp -g

gulp dev
```

### run tests

\* except e2e tests

```
npm i -g mocha

npm test
```

### run server unit tests

```
mocha test\server\models\*.spec.js
```

### run server api tests

```
mocha test\server\api\restaurants.spec.js
```

### run e2e tests

```
npm i -g protractor

webdriver-manager update

gulp dev

webdriver-manager start

protractor test\client\protractor.conf.js
```