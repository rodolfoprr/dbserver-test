'use strict';

var should				= require('should')
		, mockery 		= require('mockery')
		, Restaurant 	= require('../../../src/server/models/restaurant')
		, Vote 				= require('../../../src/server/models/vote');

var RESTAURANT_REPOSITORY_PATH = '../../../src/server/repositories/restaurants';

beforeEach(function() {
	delete require.cache[require.resolve(RESTAURANT_REPOSITORY_PATH)];
});

describe('restaurants - repository', function() {
	
	it('must return the top voted restaurant', function() {
		
		var restaurantRepository = require(RESTAURANT_REPOSITORY_PATH);
		
		var restaurant = new Restaurant(1);
		var restaurant2 = new Restaurant(2);
		
		var vote1 = new Vote('12345');
		var vote2 = new Vote('123456');
		var vote3 = new Vote('1234567');
		
		restaurantRepository.vote(restaurant, vote1);
		restaurantRepository.vote(restaurant, vote2);
		restaurantRepository.vote(restaurant2, vote3);
		
		var restaurantOfTheDay = restaurantRepository.getMostVotedToday();
		
		restaurantOfTheDay.id.should.be.exactly(1);
		
	});
	
	it('must not return a restaurant if there are restaurants with the same number of votes', function() {
		
		var restaurantRepository = require(RESTAURANT_REPOSITORY_PATH);
		
		var restaurant = new Restaurant(1);
		var restaurant2 = new Restaurant(2);
		
		var vote1 = new Vote('12345');
		var vote2 = new Vote('123456');
		var vote3 = new Vote('1234567');
		var vote4 = new Vote('12345678');
		
		restaurantRepository.vote(restaurant, vote1);
		restaurantRepository.vote(restaurant, vote2);
		restaurantRepository.vote(restaurant2, vote3);
		restaurantRepository.vote(restaurant2, vote4);
		
		var restaurantOfTheDay = restaurantRepository.getMostVotedToday();
		
		should(restaurantOfTheDay).not.be.ok();
		
	});
	
	it('must return the chosen restaurant on this day', function() {
		
		mockery.enable({
			warnOnUnregistered: false
		});
		
		var dateMock = {
			getCurrent: function () {
				var yesterdayDate = new Date();
		
				yesterdayDate.setDate(yesterdayDate.getDate() - 1);
				
				return yesterdayDate;
			}
		};
		
		mockery.registerMock('date', dateMock);
		
		var restaurantRepository = require(RESTAURANT_REPOSITORY_PATH);
		
		var restaurantOfTheDay = restaurantRepository.getChosenToday();
		
		restaurantOfTheDay.id.should.be.exactly(4);
		
	});
  
});