'use strict';

process.env.NODE_ENV = 'test'

var app = require('../../src/server/app');

var PORT = 3434;

module.exports = {
  before: _before,
	urlBase: "http://localhost:" + PORT + "/api"
};

function _before(done) {
	
	app.start(PORT, function() {
		done();
	});
	
};