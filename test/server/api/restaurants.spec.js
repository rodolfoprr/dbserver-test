'use strict';

var config    = require('../config')
    , request = require('superagent')
    , should  = require('should');

var urlBase = config.urlBase;

before(function(done) {
  config.before(done);
});

describe('api - restaurants', function() {
  
  it('must get all restaurants', function(done) {
    
    request.get(urlBase + '/restaurants').end(function(err, res) {
      
      res.body.length.should.be.exactly(7);
      
      done();
      
    });
    
  });
  
  it('must vote', function(done) {
    
    request.post(urlBase + '/restaurants/2/vote').send().end(function(err, res) {
      
      res.status.should.be.exactly(200);
      
      done();
      
    });
    
  });
  
  it('must return error if I try to vote twice in the same day', function(done) {
    
    var agent = request.agent();
    
    agent.post(urlBase + '/restaurants/2/vote').send().end(function(err, res) {
      
      res.status.should.be.exactly(200);
      
      agent.post(urlBase + '/restaurants/3/vote').send().end(function(err, res) {
        
        res.status.should.be.exactly(409);
        
        done();
        
      });
      
    });
    
  });
  
  it('must return error if I try to vote in a restaurant that doesn\'t exist', function(done) {
    
    request.post(urlBase + '/restaurants/14/vote').send().end(function(err, res) {
      
      res.status.should.be.exactly(400);
      
      done();
      
    });
    
  });
  
  it('must return error if I try to vote in a restaurant and If there is a restaurant of the day', function(done) {
    
    var agent = request.agent();
    
    agent.post(urlBase + '/restaurants/2/vote').send().end(function(err, res) {
      
      res.status.should.be.exactly(200);
      
      var date = new Date();
      
      var year = date.getFullYear();
      var month = date.getMonth() + 1;
      var day = date.getDay();
      
      agent.get(urlBase + '/restaurants/' + year + '/' + month + '/' + day).send().end(function(err, res) {
        
        res.status.should.be.exactly(200);
        
        agent.post(urlBase + '/restaurants/3/vote').send().end(function(err, res) {
      
          res.status.should.be.exactly(409);
          
          done();
          
        });
        
      });
      
    });
    
  });
  
  it('must return error if I try to vote in a restaurant that was chosen in the week', function(done) {
    
    request.post(urlBase + '/restaurants/4/vote').send().end(function(err, res) {
      
      res.status.should.be.exactly(409);
      
      done();
      
    });
    
  });
  
});