'use strict';

var should 		= require('should')
		, mockery = require('mockery');

var VOTE_PATH = '../../../src/server/models/vote';

beforeEach(function() {
	delete require.cache[require.resolve(VOTE_PATH)];
});

describe('vote', function() {
	
	it('must vote if I vote multiple times in different days', function() {
		
		mockery.enable({
			warnOnUnregistered: false
		});
		
		var dateMock = {
			getCurrent: function () {
				var tomorrowDate = new Date();
		
				tomorrowDate.setDate(tomorrowDate.getDate() + 1);
				
				return tomorrowDate;
			}
		};
		
		mockery.registerMock('date', dateMock);
		
		var Vote = require(VOTE_PATH);
		
		mockery.deregisterMock('date');
		mockery.disable();
		
		var voteToday = new Vote('hfoQv0x_wK-eOh-diiIR1fVtw3jWrcxG');
		var voteTomorrow = new Vote('hfoQv0x_wK-eOh-diiIR1fVtw3jWrcxG');
		
		var restaurants = [
			{ id: 1, name: 'Restaurant 1', votes: [] },
			{ id: 2, name: 'Restaurant 2', votes: [] }
		];
		
		restaurants[0].votes.push(voteToday);
		
		var canVoteTomorrow = !voteTomorrow.alreadyVotedToday(voteTomorrow.user, restaurants);
		
		canVoteTomorrow.should.be.exactly(true);
		
	});
	
	it('must vote if I did not vote today', function() {
		
		var Vote = require(VOTE_PATH);
		
		var vote = new Vote('hfoQv0x_wK-eOh-diiIR1fVtw3jWrcxG');
		
		var restaurants = [
			{ id: 1, name: 'Restaurant 1', votes: [] },
			{ id: 2, name: 'Restaurant 2', votes: [] }
		];
		
		var canVote = !vote.alreadyVotedToday(vote.user, restaurants);
		
		canVote.should.be.exactly(true);
		
	});
	
	it('must not vote if I have already voted today', function() {
		
		var Vote = require(VOTE_PATH);
		
		var vote = new Vote('hfoQv0x_wK-eOh-diiIR1fVtw3jWrcxG');
		
		var restaurants = [
			{ id: 1, name: 'Restaurant 1', votes: [] },
			{ id: 2, name: 'Restaurant 2', votes: [] }
		];
		
		restaurants[0].votes.push(vote);
		
		var canVote = !vote.alreadyVotedToday(vote.user, restaurants);
		
		canVote.should.be.exactly(false);
		
	});
  
});