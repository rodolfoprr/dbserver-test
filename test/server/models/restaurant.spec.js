'use strict';

var should 		 		= require('should')
		, Restaurant 	= require('../../../src/server/models/restaurant');

describe('restaurant - model', function() {
	
	describe('restaurant chosen in the week', function() {
		
		var restaurants = [
			{ id: 1, name: 'Restaurant 1', votes: [], chosenIn: [ new Date() ] },
			{ id: 2, name: 'Restaurant 2', votes: [], chosenIn: [] }
		];
		
		it('must return false if the restaurant was not chosen in the week', function() {
			
			var restaurant = new Restaurant(2);
			
			var wasChosen = restaurant.wasChosenThisWeek(restaurants);
			
			wasChosen.should.be.exactly(false);
			
		});
		
		it('must return true if the restaurant was chosen in the week', function() {
			
			var restaurant = new Restaurant(1);
			
			var wasChosen = restaurant.wasChosenThisWeek(restaurants);
			
			wasChosen.should.be.exactly(true);
			
		});
		
	});
  
});