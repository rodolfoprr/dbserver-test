'use strict';

describe('restaurants', function() {
  it('must vote in a restaurant', function() {
		
		browser.get('index.html');
    
    var firstRestaurant = element.all(by.repeater('restaurant in vm.restaurants')).get(4).$('button');
    
    firstRestaurant.click();
    
    expect(element(by.id('success-vote')).isDisplayed()).toBeTruthy();
    
  });
  
  it('must return error if I try to vote twice in the same day', function() {
		
		browser.get('index.html');
    
    var firstRestaurant = element.all(by.repeater('restaurant in vm.restaurants')).get(1).$('button');
    
    firstRestaurant.click();
    
    var secondRestaurant = element.all(by.repeater('restaurant in vm.restaurants')).get(2).$('button');
    
    secondRestaurant.click();
    
    expect(element(by.id('error')).isDisplayed()).toBeTruthy();
    
  });
  
  it('must choose subway restaurant', function() {
		
		browser.get('index.html');
    
    var subwayButton = element.all(by.repeater('restaurant in vm.restaurants')).get(4).$('button');
    
    subwayButton.click();
    
    var browser2 = browser.forkNewDriverInstance(true);
    
    var element2 = browser2.element;
    
    subwayButton = element2.all(by.repeater('restaurant in vm.restaurants')).get(4).$('button');
    
    subwayButton.click();
    
    var browser3 = browser.forkNewDriverInstance(true);
    
    var element3 = browser3.element;
    
    var johnnyRocketsButton = element3.all(by.repeater('restaurant in vm.restaurants')).get(1).$('button');
    
    johnnyRocketsButton.click();
    
    var resultButton = element(by.id('result'));
    
    resultButton.click();
    
    var result = element(by.id('success-result'));
    
    result.getText().then(function(text) {
      expect(text === 'Subway').toBeTruthy();
    });
    
  });
});