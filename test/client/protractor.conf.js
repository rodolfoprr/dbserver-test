'use strict';

exports.config = {
  seleniumAddress: 'http://localhost:4444/wd/hub',
  baseUrl: 'http://localhost:3001/',

  capabilities: {
    'browserName': 'chrome'
  },

  specs: ['e2e/*.spec.js']
};