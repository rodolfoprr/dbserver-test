(function () {
  'use strict';

  angular
    .module('app.restaurants')
    .factory('restaurantsService', restaurantsService);

  /* @ngInject */
  function restaurantsService($http) {
    var urlBase = '/api/restaurants';

    var restaurants = {
      vote: vote,
      getAll: getAll,
      result: result
    };
		
		return restaurants;

    function vote(id) {
      return $http.post(urlBase + '/' + id + '/vote');
    };
    
    function getAll() {
      return $http.get(urlBase);
    };
    
    function result() {
      var date = new Date();
      
      var year = date.getFullYear();
      var month = date.getMonth() + 1;
      var day = date.getDay();
      
      return $http.get(urlBase + '/' + year + '/' + month + '/' + day);
    };
  }

}());