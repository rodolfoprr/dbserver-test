(function () {
  'use strict';

  angular
    .module('app.restaurants')
    .run(appRun);

  /* @ngInject */
  function appRun(routehelper) {
    routehelper.configureRoutes(getRoutes());
  }

  function getRoutes() {
    return [
      {
        url: '/',
        config: {
          templateUrl: 'app/restaurants/index.html',
          controller: 'Restaurants',
          controllerAs: 'vm',
          title: 'Restaurants'
        }
      }
    ];
  }
}());
