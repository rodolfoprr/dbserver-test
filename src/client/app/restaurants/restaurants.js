(function () {
  'use strict';
  
  angular
    .module('app.restaurants')
    .controller('Restaurants', Restaurants)
    .controller('InsertRestaurant', InsertRestaurant);

  /* @ngInject */
  function Restaurants(restaurantsService) {
    var vm = this;
    
    vm.vote = vote;
    vm.result = result;
    vm.getRestaurants = getRestaurants;
    
    vm.getRestaurants();

    function getRestaurants() {
      restaurantsService.getAll()
        .success(function(result) {
  
          vm.restaurants = result;
  
        }).error(function (err) {
  
          vm.fail = true;
  
        });
    };
      
    function vote(id) {
      restaurantsService.vote(id)
        .success(function() {

          vm.success = true;

      }).error(function(err) {

          vm.success = false;
          vm.fail = true;
          vm.failMessage = err;

      });
    }
    
    function result() {
      restaurantsService.result()
        .success(function(result) {
          
          if (typeof result === 'object')
            vm.restaurantOfTheDay = result.name;
          else
            vm.restaurantOfTheDay = result;

      }).error(function(err) {
        
          console.log(err);

          vm.success = vm.restaurantOfTheDay = false;
          vm.fail = true;
          vm.failMessage = err;

      });
    }
  }
  
  /* @ngInject */
  function InsertRestaurant(restaurantsService) {
    var vm = this;
  }

}());
