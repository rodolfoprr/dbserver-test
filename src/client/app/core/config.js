(function () {
  'use strict';

  var core = angular.module('app.core');

  core.config(configure);

  /* @ngInject */
  function configure($routeProvider, $locationProvider, routehelperConfigProvider) {
    routehelperConfigProvider.config.$routeProvider = $routeProvider;
    routehelperConfigProvider.config.docTitle = 'DBServer - ';

    $locationProvider.html5Mode(true);
  }
}());