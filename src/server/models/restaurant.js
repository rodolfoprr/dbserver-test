'use strict';

var date = require('date');

module.exports = Restaurant;

function Restaurant(id, name) {
    this.id = +id;
    this.name = name;
    this.votes = [];
    this.chosenIn = [];
    
    this.wasChosenThisWeek = function(restaurants) {
      var wasChosen = false;
      var restaurantId = this.id;
      
      var today = date.getCurrent();
      var firstDayWeek = date.getFirstDayWeek(today);
      var lastDayWeek = date.getLastDayWeek(today);
      
      today.setHours(0, 0, 0, 0);
      firstDayWeek.setHours(0, 0, 0, 0);
      lastDayWeek.setHours(0, 0, 0, 0);
      
      restaurants.forEach(function(restaurant) {
        
        if (restaurant.id === restaurantId && restaurant.chosenIn.length > 0)
          wasChosen = today >= firstDayWeek && today <= lastDayWeek;
        
      });
      
      return wasChosen;
    },
    
    this.markAsChosen = function(date) {
      this.chosenIn.push(new Date());
    }
}