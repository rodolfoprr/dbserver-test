'use strict';

var date = require('date');

module.exports = Vote;

function Vote(user) {
    this.date = new Date();
    this.user = user;
    
    this.alreadyVotedToday = function(user, restaurants) {
      var alreadyVoted = false;
      
      var currentDate = date.getCurrent();
      
      restaurants.forEach(function(restaurant) {
        
        restaurant.votes.forEach(function(vote) {
          
          var isCurrentUser = user === vote.user;
          var votedToday = vote.date.setHours(0, 0, 0, 0) === currentDate.setHours(0, 0, 0, 0);
          
          if (isCurrentUser && votedToday)
            alreadyVoted = true;
          
        });
        
      });
      
      return alreadyVoted;
    }
}