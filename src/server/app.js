'use strict';

var express		= require('express')
		, app			= express()
		, session = require('express-session')
		, http		= require('http')
		, path		= require('path')
		, routes	= require('./api/routes');
		
var basePath = path.join(__dirname, '..', 'client');

app.use(session({
    secret: 'QQWpWMcLp0AOa64uhzLyvRRqHinudYmtBzd5FdWL',
		resave: false,
    saveUninitialized: true
}));
		
app.use(express.static(basePath));

app.use('/api', routes);

app.get('/*', function(req, res) {
	res.sendFile(basePath + "/index.html");
});

if (app.get('env') !== 'test') _start(3000)

module.exports = {
  start: _start
};
	
function _start(port, done) {
	
	port = process.env.PORT || port;
	
	http.Server(app).listen(port, function() {
		console.log('listening on localhost:' + port);
		
		if (done) done();
	});
	
}