'use strict';

var Restaurant 	= require('../models/restaurant')
		, date 			= require('date')
		, sortBy 		= require('sort-by');

var restaurants = [
	new Restaurant(1, 'Outback'),
	new Restaurant(2, 'Johnny Rockets'),
	new Restaurant(3, 'Burger King'),
	new Restaurant(4, 'Chipotle'),
	new Restaurant(5, 'Subway'),
	new Restaurant(6, 'The Restaurant at the End of the Universe'),
	new Restaurant(7, 'Farmhouse Kings Cross')
];

chooseOne();

function chooseOne() {
	var date = new Date();
	
	date.setDate(date.getDate() - 1);
	
	restaurants[3].chosenIn.push(date);
}

var restaurantRepository = {
	getAll: function() {
		
		return restaurants;
		
	},
	getMostVotedToday: function() {
		
		var restaurantsPosition = [];
		var currentDate = date.getCurrent();
		var votes = 0;
		
		restaurants.forEach(function(restaurant, i) {
			
			votes = 0;
			restaurantsPosition.push({ id: restaurant.id, name: restaurant.name  });
			
			restaurant.votes.forEach(function(vote) {
				
				var votedToday = vote.date.setHours(0, 0, 0, 0) === currentDate.setHours(0, 0, 0, 0);
				
				if (votedToday) 
					votes++;
			});
			
			restaurantsPosition[i].votes = votes;
			
		});
		
		restaurantsPosition.sort(sortBy('-votes'));
		
		if (restaurantsPosition[0].votes === restaurantsPosition[1].votes)
			return null;
		
		return restaurantsPosition[0];
		
	},
	getChosenToday: function() {
		
		var chosenRestaurant = null;		
		var currentDate = date.getCurrent();
		
		currentDate.setHours(0, 0, 0, 0);
		
		restaurants.forEach(function(restaurant, i) {
			
			restaurant.chosenIn.forEach(function(date) {
				
				date.setHours(0, 0, 0, 0);
				
				if (date.valueOf() === currentDate.valueOf()) {
					chosenRestaurant = restaurant;
				}
				
			});
			
		});
		
		return chosenRestaurant;
		
	},
	vote: function(restaurant, vote) {
		
		var restaurantsLength = restaurants.length;
		
		for (var i = 0; i < restaurantsLength; i++) {
			
			if (restaurants[i].id === +restaurant.id)
				restaurants[i].votes.push(vote);
			
		}
		
	}
}

module.exports = restaurantRepository;