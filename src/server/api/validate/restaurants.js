'use strict';

var restaurantsValidate = {
	vote: function(req, res, next) {
		
		var restaurant = {
			id: req.params.id
		};
		
		if (restaurant.id < 0 || restaurant.id > 7)
			return res.status(400).json('This restaurant doesn\'t exist.');
	
		next();
	
	}
};

module.exports = restaurantsValidate;