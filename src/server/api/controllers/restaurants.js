'use strict';

var Vote 										= require('../../models/vote')
		, Restaurant 						= require('../../models/restaurant')
		, restaurantRepository 	= require('../../repositories/restaurants');

var restaurantController = {
	getAll: function(req, res) {
		
		var restaurants = restaurantRepository.getAll();
		
		restaurants.forEach(function(restaurant) {
			
			restaurant.canVote = !restaurant.wasChosenThisWeek(restaurants);
			
		});
		
		res.json(restaurants);
	
	},
	getOfTheDay: function(req, res) {
		
		var restaurantOfTheDay = restaurantRepository.getChosenToday();
		
		if (restaurantOfTheDay)
			return res.json(restaurantOfTheDay);		
		
		restaurantOfTheDay = restaurantRepository.getMostVotedToday();
		
		if (!restaurantOfTheDay)
			return res.json('There was a tie for first place.');
		
		var restaurants = restaurantRepository.getAll();
		
		restaurants.forEach(function(restaurant) {
			
			if (restaurant.id === restaurantOfTheDay.id)
				restaurant.markAsChosen();
			
		});
		
		res.json(restaurantOfTheDay);
	
	},
	vote: function(req, res) {
		
		var restaurantId = +req.params.id;		
		var restaurants = restaurantRepository.getAll();
		
		var vote = new Vote(req.sessionID);
		var restaurant = new Restaurant(restaurantId);
		
		var alreadyVotedToday = vote.alreadyVotedToday(vote.user, restaurants);
		var restaurantOfTheDay = restaurantRepository.getChosenToday();
		
		if (alreadyVotedToday)
			return res.status(409).send('You have already voted today.');
			
		if (restaurantOfTheDay)
			return res.status(409).send('You can\'t vote today because a restaurant has already been chosen.');
		
		restaurantRepository.vote(restaurant, vote);
	
		res.json(vote);
	
	}
};

module.exports = restaurantController;