'use strict';

var express = require('express');

var router = express.Router();

var restaurantsRoutes = {
  controller: require('./controllers/restaurants'),
  validate: require('./validate/restaurants')
};

router
  .get('/restaurants', restaurantsRoutes.controller.getAll)
  .get('/restaurants/:year/:month/:day', restaurantsRoutes.controller.getOfTheDay)
  .post('/restaurants/:id/vote', restaurantsRoutes.validate.vote, restaurantsRoutes.controller.vote)
  .get('/restaurants/*', function(req, res) { res.status(404).send('Not found.'); });

module.exports = router;