var gulp          = require('gulp')
    , paths       = require('./gulp.config.json')
    , plugins     = require('gulp-load-plugins')()
    , utils       = plugins.loadUtils(['log', 'colors'])
    , nodemon     = require('gulp-nodemon')
		, browserSync = require('browser-sync')
    , del         = require('del')
    , reload      = browserSync.reload;
    
gulp.task('start-server', function () {
  nodemon({ script: paths.build.server.app });
});

gulp.task('delete', function () {
  utils.log('Deleting: ' + utils.colors.black.bgGreen(paths.build.root));

  var delPaths = [].concat(paths.build.root);

  del.sync(delPaths);
});

gulp.task('server-js', function() {
  utils.log(utils.colors.black.bgGreen('Server - JS'));
  
  gulp.src(paths.src.server.js)
    .pipe(plugins.changed(paths.build.server.js))
    .pipe(gulp.dest(paths.build.server.js))
    .pipe(reload({stream: true}));
});

gulp.task('client-html', function() {
  utils.log(utils.colors.black.bgGreen('Client - HTML'));
  
  gulp.src(paths.src.client.html.root)
    .pipe(plugins.changed(paths.build.client.html.root))
    .pipe(gulp.dest(paths.build.client.html.root))
    .pipe(reload({stream: true}));

  gulp.src(paths.src.client.html.app)
    .pipe(plugins.changed(paths.build.client.html.app))
    .pipe(gulp.dest(paths.build.client.html.app))
    .pipe(reload({stream: true}));
});

gulp.task('client-js', function() {
  utils.log(utils.colors.black.bgGreen('Client - JS'));
  
  gulp.src(paths.src.client.js.vendor)
    .pipe(plugins.changed(paths.build.client.js.vendor))
    .pipe(gulp.dest(paths.build.client.js.vendor))
    .pipe(reload({stream: true}));

  gulp.src(paths.src.client.js.app)
    .pipe(plugins.changed(paths.build.client.js.app))
    .pipe(plugins.ngAnnotate({
      add: true,
      single_quotes: true
    }))
    .pipe(gulp.dest(paths.build.client.js.app))
    .pipe(reload({stream: true}));
});


gulp.task('b-s', function() {
  browserSync.init({
    proxy: 'localhost:3000',
    ghostMode: false
  });
  
});

gulp.task('dev', ['delete', 'server-js', 'client-js', 'client-html', 'start-server', 'b-s'], function() {
  gulp.watch(paths.src.server.js, ['server-js']);
  gulp.watch(paths.src.client.html.root, ['client-html']);
  gulp.watch(paths.src.client.html.app, ['client-html']);
  gulp.watch(paths.src.client.js.app, ['client-js']);
});

gulp.task('prod', ['delete', 'server-js', 'client-js', 'client-html'], function() {
  
  gulp.src(paths.src.prod.packages).pipe(gulp.dest(paths.build.root));
  gulp.src(paths.src.prod.node_modules).pipe(gulp.dest(paths.build.node_modules));
    
});